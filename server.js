const express = require('express');
const app = express();
const port = 3000;
const copydir = require('copy-dir');

console.log('Copying ionic');
copydir.sync('./node_modules/@ionic/core', './public/ionic/core');
console.log('Copying done');

app.use(express.static('public'));

app.listen(port, function() {console.log(`Server listening on port ${port}`)});

